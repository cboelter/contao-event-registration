<?php

// Models
$GLOBALS['TL_MODELS']['tl_calendar_events_registration'] =
    'Boelter\\EventRegistration\\Model\\CalendarEventsRegistrationModel';

// Module-Config
$GLOBALS['BE_MOD']['content']['calendar']['tables'][]            = 'tl_calendar_events_registration';
$GLOBALS['BE_MOD']['content']['calendar']['export_registration'] =
    \Boelter\EventRegistration\Dca\CalendarEventsRegistration::callback('generateRegistrationExport');
$GLOBALS['BE_MOD']['content']['calendar']['stylesheet']          =
    'system/modules/event-registration/assets/css/backend.css';

// FrontendModule-Config
$GLOBALS['FE_MOD']['events']['eventreader'] = 'Boelter\\EventRegistration\\Module\\EventReader';
$GLOBALS['FE_MOD']['events']['eventoptout'] = 'Boelter\\EventRegistration\\Module\\EventOptOut';

// FormField-Config
$GLOBALS['TL_FFL']['add_attendee'] = 'Boelter\\EventRegistration\\Forms\\AddAttendee';

// NotificationCenter-Config
$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['contao']['event_registration_main_attendee'] = array(
    'recipients'           => array('admin_email', 'form_*', 'attendee_*'),
    'email_subject'        => array('event_*', 'attendee_*'),
    'email_text'           => array('event_*', 'attendee_*', 'optout'),
    'email_html'           => array('event_*', 'attendee_*', 'optout'),
    'email_sender_name'    => array('admin_email'),
    'email_sender_address' => array('admin_email'),
    'email_recipient_cc'   => array('admin_email'),
    'email_recipient_bcc'  => array('admin_email'),
    'email_replyTo'        => array('admin_email'),
);

$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['contao']['event_registration_attendee'] = array(
    'recipients'           => array('admin_email', 'form_*', 'attendee_*', 'sub_attendee_*'),
    'email_subject'        => array('event_*', 'attendee_*'),
    'email_text'           => array('event_*', 'attendee_*', 'optout', 'sub_attendee_*'),
    'email_html'           => array('event_*', 'attendee_*', 'optout', 'sub_attendee_*'),
    'email_sender_name'    => array('admin_email'),
    'email_sender_address' => array('admin_email'),
    'email_recipient_cc'   => array('admin_email'),
    'email_recipient_bcc'  => array('admin_email'),
    'email_replyTo'        => array('admin_email'),
);

$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['contao']['event_registration_optout'] = array(
    'recipients'           => array('admin_email', 'attendee_*'),
    'email_subject'        => array('event_*', 'attendee_*'),
    'email_text'           => array('event_*', 'attendee_*'),
    'email_html'           => array('event_*', 'attendee_*'),
    'email_sender_name'    => array('admin_email'),
    'email_sender_address' => array('admin_email'),
    'email_recipient_cc'   => array('admin_email'),
    'email_recipient_bcc'  => array('admin_email'),
    'email_replyTo'        => array('admin_email'),
);
