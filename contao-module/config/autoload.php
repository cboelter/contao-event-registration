<?php

TemplateLoader::addFiles(
    array
    (
        'mod_event_registration'       => 'system/modules/event-registration/templates/modules',
        'mod_event_optout'             => 'system/modules/event-registration/templates/modules',
        'form_add_attendee'            => 'system/modules/event-registration/templates/forms',
        'form_textfield_registration'  => 'system/modules/event-registration/templates/forms',
        'form_add_attendee_subwidgets' => 'system/modules/event-registration/templates/forms',
    )
);