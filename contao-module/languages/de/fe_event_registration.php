<?php

$GLOBALS['TL_LANG']['fe_event_registration']['add_attendee_label']           = 'Weitere Teilnehmer hinzufügen';
$GLOBALS['TL_LANG']['fe_event_registration']['register']                     = 'Anmeldung absenden';
$GLOBALS['TL_LANG']['fe_event_registration']['clone']                        = 'Teilnehmer hinzufügen';
$GLOBALS['TL_LANG']['fe_event_registration']['delete']                       = 'Teilnehmer löschen';
$GLOBALS['TL_LANG']['fe_event_registration']['notAvailableError']            = '<p>Leider schon ausgebucht</p>';
$GLOBALS['TL_LANG']['fe_event_registration']['notAvailableAfterSubmitError'] =
    '<p>Leider nicht mehr ausreichend Plätze vorhanden. Es gibt noch %s Plätze.</p>';
$GLOBALS['TL_LANG']['fe_event_registration']['optout_error_hash']            =
    '<p>Es wurde kein Hash übergeben, keine Abmeldung möglich.</p>';
$GLOBALS['TL_LANG']['fe_event_registration']['optout_error']                 =
    '<p>Es wurde keine Registrierung gefunden.</p>';
$GLOBALS['TL_LANG']['fe_event_registration']['optout_success']               =
    '<p>Sie wurden erfolgreich vom Event %s abgemeldet.</p>';