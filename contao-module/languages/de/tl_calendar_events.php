<?php

// Buttons
$GLOBALS['TL_LANG']['tl_calendar_events']['event_registration']        =
    array('Event Registrierung', 'Die Registrierungen für das Event ID %s ansehen');
$GLOBALS['TL_LANG']['tl_calendar_events']['event_registration_export'] =
    array('Event Registrierung Export', 'Die Registrierungen für das Event ID %s exportieren');

// Legends
$GLOBALS['TL_LANG']['tl_calendar_events']['event_registration_legend'] = 'Event-Registrierung';

// Fields
$GLOBALS['TL_LANG']['tl_calendar_events']['er_add_registration']         = array('Registrierung aktivieren');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_headline']                 = array('Überschrift');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_intro']                    = array('Einleitungstext');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_max']                      =
    array('Maximale Anzahl Plätze', 'Bitte geben Sie hier die maximale Anzahl an PLätzen ein. 0 = ohne Begrenzung');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_register_multiple']        = array('Registrierung + X aktivieren');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_register_multiple_fields'] = array('Felder für Registrierung + X');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_notification']             = array('Benachrichtigung');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_notification_attendee']    =
    array('Benachrichtigung hinzugefügter Teilnehmer');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_success']                  = array('Weiterleitungs-Seite');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_success_optout']           = array('Weiterleitungs-Seite Opt-Out');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_start']                    = array('Anmeldung von');
$GLOBALS['TL_LANG']['tl_calendar_events']['er_stop']                     = array('Anmeldung bis');
