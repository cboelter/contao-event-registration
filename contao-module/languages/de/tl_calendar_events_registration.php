<?php

// Legend
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['person_legend']  = 'Registrierungs-Daten';
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['contact_legend'] = 'Kontakt-Daten';
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['info_legend']    = 'Registrierungs-Infos';

// Fields
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['firstname']               = array('Vorname');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['lastname']                = array('Nachname');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['company']                 = array('Firma');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['email']                   = array('E-Mail');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['phone']                   = array('Telefon');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['registration_tstamp']     = array('Registriert am');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['current_registrations']   = array('Registrierungen gesamt');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['available_registrations'] =
    array('Registrierungen verbleibend');

// Buttons
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['export_registration'] = array('Registrierungen exportieren');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['new']                 = array('Neue Registrierung');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['edit']                =
    array('Registrierung bearbeiten', 'Die Registrierung ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['delete']              =
    array('Registrierung löschen', 'Die Registrierung ID %s löschen');
$GLOBALS['TL_LANG']['tl_calendar_events_registration']['show']                =
    array('Registrierung anzeigen', 'Die Registrierung ID %s anzeigen');