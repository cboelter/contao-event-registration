<?php

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['event_registration_main_attendee']['0'] =
    'Event Registrierung Hauptteilnehmer';

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['event_registration_attendee']['0'] =
    'Event Registrierung Teilnehmer';

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['event_registration_optout']['0'] =
    'Event Registrierung Abmeldung';