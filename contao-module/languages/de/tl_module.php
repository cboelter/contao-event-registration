<?php

// Palettes
$GLOBALS['TL_LANG']['tl_module']['event_registration_legend'] = 'Event Registrierung-Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['event_optout_legend']       = 'Event Registrierung-Opt-out';

// Fields
$GLOBALS['TL_LANG']['tl_module']['er_headline']                 = array('Überschrift');
$GLOBALS['TL_LANG']['tl_module']['er_intro']                    = array('Einleitungstext');
$GLOBALS['TL_LANG']['tl_module']['er_success']                  = array('Weiterleitungs-Seite');
$GLOBALS['TL_LANG']['tl_module']['er_success_optout']           = array('Weiterleitungs-Seite Opt-Out');
$GLOBALS['TL_LANG']['tl_module']['er_register_multiple_fields'] = array('Felder für Registrierung + X');
$GLOBALS['TL_LANG']['tl_module']['er_notification']             = array('Benachrichtigung');
$GLOBALS['TL_LANG']['tl_module']['er_notification_attendee']    = array('Benachrichtigung hinzugefügter Teilnehmer');
$GLOBALS['TL_LANG']['tl_module']['er_optout_notification']      = array('Benachrichtigung abgemeldeter Teilnehmer');