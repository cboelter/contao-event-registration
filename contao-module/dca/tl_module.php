<?php

// Palettes
$GLOBALS['TL_DCA']['tl_module']['palettes']['eventreader'] .= ';{event_registration_legend},er_headline,er_intro,er_success,er_success_optout,er_register_multiple_fields,er_notification,er_notification_attendee';
$GLOBALS['TL_DCA']['tl_module']['palettes']['eventoptout'] =
    '{title_legend},name,headline,type;{event_optout_legend},er_optout_notification';

// Fields
$GLOBALS['TL_DCA']['tl_module']['fields']['er_headline'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['er_headline'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array(
        'tl_class'  => 'long',
        'mandatory' => true,
        'maxlength' => 255,
    ),
    'sql'       => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_intro'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['er_intro'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array(
        'tl_class'  => 'long',
        'mandatory' => true,
        'rte'       => 'tinyMCE'
    ),
    'sql'       => "text NOT NULL",
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_success'] = array(
    'label'      => &$GLOBALS['TL_LANG']['tl_module']['er_success'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => array(
        'fieldType' => 'radio',
        'tl_class'  => 'long',
        'mandatory' => true,
    ),
    'sql'        => "int(10) unsigned NOT NULL default '0'",
    'relation'   => array(
        'type' => 'hasOne',
        'load' => 'eager'
    )
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_success_optout'] = array(
    'label'      => &$GLOBALS['TL_LANG']['tl_module']['er_success_optout'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => array(
        'fieldType' => 'radio',
        'tl_class'  => 'long',
        'mandatory' => true,
    ),
    'sql'        => "int(10) unsigned NOT NULL default '0'",
    'relation'   => array(
        'type' => 'hasOne',
        'load' => 'eager'
    )
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_register_multiple_fields'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['er_register_multiple_fields'],
    'inputType'        => 'checkboxWizard',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getRegisterMultipleFields'),
    'eval'             => array(
        'tl_class' => 'clr long',
        'multiple' => true,
    ),
    'sql'              => "varchar(255) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['er_notification_attendee'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['er_notification_attendee'],
    'inputType'        => 'select',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getNotificationsAttendee'),
    'eval'             => array(
        'tl_class'  => 'w50',
        'mandatory' => true,
        'chosen'    => true,
    ),
    'sql'              => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_notification'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['er_notification'],
    'inputType'        => 'select',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getNotifications'),
    'eval'             => array(
        'tl_class'  => 'clr w50',
        'mandatory' => true,
        'chosen'    => true,
    ),
    'sql'              => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['er_optout_notification'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['er_optout_notification'],
    'inputType'        => 'select',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\Module::callback('getOptOutNotifications'),
    'eval'             => array(
        'tl_class'  => 'clr w50',
        'mandatory' => true,
        'chosen'    => true,
    ),
    'sql'              => "int(10) unsigned NOT NULL default '0'"
);