<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Load tl_content language file
 */
System::loadLanguageFile('tl_content');


/**
 * Table tl_calendar_events_registration
 */
$GLOBALS['TL_DCA']['tl_calendar_events_registration'] = array
(

    // Config
    'config'       => array
    (
        'dataContainer'    => 'Table',
        'ptable'           => 'tl_calendar_events',
        'switchToEdit'     => true,
        'enableVersioning' => true,
        'sql'              => array
        (
            'keys' => array
            (
                'id' => 'primary',
            )
        )
    ),
    // MetaPalettes
    'metapalettes' => array
    (
        'default' => array
        (
            'person'  => array(
                'firstname',
                'lastname'
            ),
            'contact' => array(
                'company',
                'email',
                'phone'
            ),
            'info'    => array(
                ':hide',
                'registration_tstamp',
            )
        )
    ),
    // List
    'list'         => array
    (
        'sorting'           => array
        (
            'mode'                  => 4,
            'fields'                => array(
                'id ASC'
            ),
            'headerFields'          => array(
                'title',
                'jumpTo',
                'tstamp',
                'er_max',
                'er_test',
            ),
            'panelLayout'           => 'filter;sort,search,limit',
            'child_record_callback' => \Boelter\EventRegistration\Dca\CalendarEventsRegistration::callback(
                'listEventRegistrations'
            ),
            'header_callback'       => \Boelter\EventRegistration\Dca\CalendarEventsRegistration::callback(
                'modifyTableHeader'
            ),
            'disableGrouping'       => true
        ),
        'global_operations' => array(
            'export_registration' => array
            (
                'label'      => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['export_registration'],
                'href'       => 'key=export_registration',
                'class'      => 'header_registration_export',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ),
        ),
        'operations'        => array
        (
            'edit'   => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['edit'],
                'href'  => 'act=edit',
                'icon'  => 'edit.gif'
            ),
            'delete' => array
            (
                'label'      => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['delete'],
                'href'       => 'act=delete',
                'icon'       => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm']
                                . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show'   => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['show'],
                'href'  => 'act=show',
                'icon'  => 'show.gif'
            )
        )
    ),
    // Fields
    'fields'       => array
    (
        'id'                  => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid'                 => array
        (
            'foreignKey' => 'tl_calendar_events.title',
            'sql'        => "int(10) unsigned NOT NULL default '0'",
            'relation'   => array(
                'type' => 'belongsTo',
                'load' => 'eager'
            ),
        ),
        'tstamp'              => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'optout_hash'         => array
        (
            'sql' => "varchar(32) NOT NULL default ''"
        ),
        'firstname'           => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['firstname'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => array(
                'mandatory'          => true,
                'maxlength'          => 55,
                'tl_class'           => 'w50',
                'haste_csv_position' => 2,
                'feEditable'         => true,
            ),
            'sql'       => "varchar(55) NOT NULL default ''"
        ),
        'lastname'            => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['lastname'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => array(
                'mandatory'          => true,
                'maxlength'          => 55,
                'tl_class'           => 'w50',
                'haste_csv_position' => 3,
                'feEditable'         => true,
            ),
            'sql'       => "varchar(55) NOT NULL default ''"
        ),
        'company'             => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['company'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'text',
            'eval'      => array(
                'maxlength'          => 25,
                'tl_class'           => 'w50',
                'haste_csv_position' => 6,
                'feEditable'         => true,
            ),
            'sql'       => "varchar(25) NOT NULL default ''"
        ),
        'email'               => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['email'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => array(
                'mandatory'          => true,
                'maxlength'          => 100,
                'rgxp'               => 'email',
                'tl_class'           => 'w50',
                'haste_csv_position' => 4,
                'feEditable'         => true,
            ),
            'sql'       => "varchar(100) NOT NULL default ''"
        ),
        'phone'               => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['phone'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => array(
                'maxlength'          => 25,
                'rgxp'               => 'phone',
                'tl_class'           => 'w50',
                'haste_csv_position' => 5,
                'feEditable'         => true,
            ),
            'sql'       => "varchar(25) NOT NULL default ''"
        ),
        'registration_tstamp' => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events_registration']['registration_tstamp'],
            'sorting'   => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => array(
                'tl_class'           => 'w50',
                'readonly'           => true,
                'rgxp'               => 'datim',
                'haste_csv_position' => 1,
            ),
            'sql'       => "int(10) unsigned NOT NULL default '0'"
        ),
    )
);