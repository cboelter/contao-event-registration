<?php

// Config
$GLOBALS['TL_DCA']['tl_calendar_events']['config']['ctable'][] = 'tl_calendar_events_registration';

// Operations
$GLOBALS['TL_DCA']['tl_calendar_events']['list']['operations']['event_registration'] = array(
    'label'           => &$GLOBALS['TL_LANG']['tl_calendar_events']['event_registration'],
    'href'            => 'table=tl_calendar_events_registration',
    'icon'            => 'system/modules/event-registration/assets/images/registration.gif',
    'button_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('generateEventRegistrationButton')
);

$GLOBALS['TL_DCA']['tl_calendar_events']['list']['operations']['event_registration_export'] = array(
    'label'           => &$GLOBALS['TL_LANG']['tl_calendar_events']['event_registration_export'],
    'href'            => 'key=export_registration',
    'icon'            => 'system/modules/event-registration/assets/images/registration_export.gif',
    'button_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback(
        'generateExportRegistrationButton'
    )
);

// Palettes
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default'] .= ';{event_registration_legend},er_add_registration';

// Subpalettes
$GLOBALS['TL_DCA']['tl_calendar_events']['metasubpalettes'] = array(
    'er_add_registration'  => array(
        'er_headline',
        'er_intro',
        'er_success',
        'er_success_optout',
        'er_max',
        'er_notification',
        'er_register_multiple',
        'er_start',
        'er_stop',
    ),
    'er_register_multiple' => array(
        'er_register_multiple_fields',
        'er_notification_attendee'
    )
);

// Fields
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_add_registration'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_add_registration'],
    'inputType' => 'checkbox',
    'exclude'   => true,
    'eval'      => array(
        'tl_class'       => 'w50 m12',
        'submitOnChange' => true
    ),
    'sql'       => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_headline'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_headline'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array(
        'tl_class'  => 'clr long',
        'maxlength' => 255,
    ),
    'sql'       => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_intro'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_intro'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => array(
        'tl_class' => 'long',
        'rte'      => 'tinyMCE'
    ),
    'sql'       => "text NOT NULL",
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_max'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_max'],
    'inputType' => 'text',
    'exclude'   => true,
    'eval'      => array(
        'tl_class' => 'w50',
    ),
    'sql'       => "int(4) NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_register_multiple'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_register_multiple'],
    'inputType' => 'checkbox',
    'exclude'   => true,
    'eval'      => array(
        'tl_class'       => 'clr w50 m12',
        'submitOnChange' => true,
    ),
    'sql'       => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_register_multiple_fields'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_register_multiple_fields'],
    'inputType'        => 'checkboxWizard',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getRegisterMultipleFields'),
    'eval'             => array(
        'tl_class' => 'clr w50',
        'multiple' => true,
    ),
    'sql'              => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_notification_attendee'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_notification_attendee'],
    'inputType'        => 'select',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getNotificationsAttendee'),
    'eval'             => array(
        'tl_class'           => 'w50',
        'chosen'             => true,
        'includeBlankOption' => true,
    ),
    'sql'              => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_notification'] = array(
    'label'            => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_notification'],
    'inputType'        => 'select',
    'exclude'          => true,
    'options_callback' => \Boelter\EventRegistration\Dca\CalendarEvents::callback('getNotifications'),
    'eval'             => array(
        'tl_class'           => 'w50',
        'chosen'             => true,
        'includeBlankOption' => true,
    ),
    'sql'              => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_success'] = array(
    'label'      => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_success'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => array(
        'fieldType' => 'radio',
        'tl_class'  => 'clr long',
    ),
    'sql'        => "int(10) unsigned NOT NULL default '0'",
    'relation'   => array(
        'type' => 'hasOne',
        'load' => 'eager'
    )
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_success_optout'] = array(
    'label'      => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_success_optout'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => array(
        'fieldType' => 'radio',
        'tl_class'  => 'long',
    ),
    'sql'        => "int(10) unsigned NOT NULL default '0'",
    'relation'   => array(
        'type' => 'hasOne',
        'load' => 'eager'
    )
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_start'] = array(
    'exclude'   => true,
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_start'],
    'inputType' => 'text',
    'eval'      => array(
        'rgxp'       => 'datim',
        'datepicker' => true,
        'tl_class'   => 'clr w50 wizard'
    ),
    'sql'       => "varchar(10) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['er_stop'] = array(
    'exclude'   => true,
    'label'     => &$GLOBALS['TL_LANG']['tl_calendar_events']['er_stop'],
    'inputType' => 'text',
    'eval'      => array(
        'rgxp'       => 'datim',
        'datepicker' => true,
        'tl_class'   => 'w50 wizard'
    ),
    'sql'       => "varchar(10) NOT NULL default ''"
);