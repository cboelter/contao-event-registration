<?php

namespace Boelter\EventRegistration\Forms;

use Haste\Util\Debug;

class AddAttendee extends \Widget
{

    /**
     * Submit user input
     *
     * @var boolean
     */
    protected $blnSubmitInput = true;

    /**
     * Add a for attribute
     *
     * @var boolean
     */
    protected $blnForAttribute = true;

    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'form_add_attendee';

    /**
     * The CSS class prefix
     *
     * @var string
     */
    protected $strPrefix = 'widget widget-add_attendee';

    protected $subWidgets = array();

    protected $renderedSubWidgets = array();

    protected $strFieldPrefix = 'attendee_';

    protected $activateAddAttendee = false;


    /**
     * Initialize the object
     */
    public function __construct($arrAttributes = false)
    {
        $this->addAttributes($arrAttributes);

        if (\Input::post('activate_add_attendee')) {
            $this->activateAddAttendee = true;
            return array();
        }

        parent::__construct();
    }

    /**
     * Add specific attributes
     *
     * @param string $strKey   The attribute key
     * @param mixed  $varValue The attribute value
     */
    public function __set($strKey, $varValue)
    {
        switch ($strKey) {
            case 'maxlength':
                if ($varValue > 0) {
                    $this->arrAttributes['maxlength'] = $varValue;
                }
                break;

            case 'mandatory':
                if ($varValue) {
                    $this->arrAttributes['required'] = 'required';
                } else {
                    unset($this->arrAttributes['required']);
                }
                parent::__set($strKey, $varValue);
                break;

            case 'min':
            case 'max':
            case 'step':
            case 'placeholder':
                $this->arrAttributes[$strKey] = $varValue;
                break;

            default:
                parent::__set($strKey, $varValue);
                break;
        }
    }

    /**
     * Trim the values
     *
     * @param mixed $varInput The user input
     *
     * @return mixed The validated user input
     */
    public function validator($varInput)
    {
        if (!\Input::post('activate_add_attendee')) {
            return array();
        }

        $this->generateSubWidgets();

        $errors = false;
        foreach ($varInput as $key => $value) {
            foreach ($this->subWidgets as $widget) {
                $renderWidget           = clone $widget;
                $renderWidget->rowCount = $key;
                $renderWidget->value    = $value[$renderWidget->name];
                $renderWidget->validator($value[$renderWidget->name]);

                if ($renderWidget->hasErrors()) {
                    $errors = true;
                }

                $this->renderedSubWidgets[$key][$renderWidget->name] = $renderWidget;
            }
        }

        if ($errors) {
            $this->addError('');
        }

        return $varInput;
    }

    public function generateLabel()
    {
        if ($this->strLabel == '') {
            return '';
        }

        return $this->strLabel;
    }

    /**
     * Generate the widget and return it as string
     *
     * @return string The widget markup
     */
    public function generate()
    {
        $GLOBALS['TL_JAVASCRIPT'][] =
            Debug::uncompressedFile('system/modules/event-registration/assets/js/jquery-cloneya.min.js');

        if (!$this->varValue) {
            $this->generateSubWidgets();
        }

        $renderedTemplate = '';

        if ($this->varValue && \Input::post('activate_add_attendee')) {
            foreach ($this->renderedSubWidgets as $key => $value) {
                $widgets           = array();
                $subWidgetTemplate = new \FrontendTemplate('form_add_attendee_subwidgets');
                foreach ($this->renderedSubWidgets[$key] as $widget) {
                    $widgets[] = $widget->parse();
                }

                $subWidgetTemplate->setData(array('subWidgets' => $widgets));
                $renderedTemplate .= $subWidgetTemplate->parse();
            }
        } else {
            $widgets           = array();
            $subWidgetTemplate = new \FrontendTemplate('form_add_attendee_subwidgets');

            foreach ($this->subWidgets as $widget) {
                $widget->rowCount = 0;
                $widgets[]        = $widget->parse();
            }

            $subWidgetTemplate->setData(array('subWidgets' => $widgets));
            $renderedTemplate = $subWidgetTemplate->parse();
        }

        return $renderedTemplate;
    }

    protected function generateSubWidgets()
    {
        $subFields = $this->columnFields;

        foreach ($subFields as $field => $config) {
            $class              = $GLOBALS['TL_FFL'][$config['inputType']];
            $widget             = new $class(\Widget::getAttributesFromDca($config, $this->generateFieldName($field)));
            $widget->customTpl  = 'form_textfield_registration';
            $widget->tableless  = true;
            $widget->parentName = $this->name;

            $this->subWidgets[$this->generateFieldName($field)] = $widget;
        }
    }

    protected function generateFieldName($name)
    {
        return $this->strFieldPrefix . $name;
    }
}