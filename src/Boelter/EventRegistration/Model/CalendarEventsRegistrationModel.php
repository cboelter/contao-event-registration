<?php

namespace Boelter\EventRegistration\Model;

class CalendarEventsRegistrationModel extends \Model
{

    /**
     * Table name
     *
     * @var string
     */
    protected static $strTable = 'tl_calendar_events_registration';

    public static function findByPid($intPid, array $arrOptions = array())
    {
        $t          = static::$strTable;
        $arrColumns = array("$t.pid=?");
        $arrValues  = array($intPid);

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.sorting";
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }

    public static function findByHash($hash, array $arrOptions = array())
    {
        $t          = static::$strTable;
        $arrColumns = array("$t.optout_hash=?");
        $arrValues  = array($hash);

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }
}