<?php

namespace Boelter\EventRegistration\Module;

use Boelter\EventRegistration\Model\CalendarEventsRegistrationModel;
use Haste\Form\Form;
use Haste\Util\StringUtil;
use NotificationCenter\Model\Notification;

class EventReader extends \ModuleEventReader
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_event_registration';

    protected function compile()
    {
        parent::compile();
        \Controller::loadLanguageFile('fe_event_registration');
        $event     =
            \CalendarEventsModel::findPublishedByParentAndIdOrAlias(\Input::get('events'), $this->cal_calendar);
        $available = true;

        if ($event === null || !$event->er_add_registration) {
            return false;
        }

        $form          = $this->generateForm($event);
        $registrations = $this->getDatabase()
            ->prepare("SELECT COUNT(*) as currentRegistration FROM tl_calendar_events_registration Where pid = ?")
            ->execute(
                $event->id
            );

        if (!$form->isSubmitted() && $registrations->currentRegistration == $event->er_max) {
            $this->Template->notAvailableError = $GLOBALS['TL_LANG']['fe_event_registration']['notAvailableError'];
            return;
        }

        $newRegistrations = 0;

        if ($form->validate() && $available) {
            if ($event->er_register_multiple && count($form->fetch('add_attendees')) > 0) {

                $submitted =
                    array_filter(
                        $form->fetch('add_attendees')[0],
                        function ($v) {
                            return !empty($v);
                        }

                    );

                if ($submitted) {
                    $newRegistrations = (int) $event->er_register_multiple ? count(\Input::post('add_attendees')) : 0;
                }
            }

            $newRegistrations = $newRegistrations + 1;
            if (($registrations->currentRegistration + $newRegistrations) > $event->er_max) {
                $this->Template->notAvailableAfterSubmitError = sprintf(
                    $GLOBALS['TL_LANG']['fe_event_registration']['notAvailableAfterSubmitError'],
                    ($event->er_max
                     - $registrations->currentRegistration)
                );

                return;
            } else {
                $this->createRegistration($form->fetchAll(), $form, $event);
            }
        }

        $this->Template->er_add_registration = $event->er_add_registration;
        $this->Template->er_headline         = $event->er_headline ?: $this->er_headline;
        $this->Template->er_intro            = $event->er_intro ?: $this->er_intro;
        $this->Template->registrationForm    = $form->generate();
    }

    protected function generateForm(\CalendarEventsModel $event)
    {
        \Controller::loadLanguageFile('fe_event_registration');

        $form = new Form(
            'event_registration_' . $this->id, 'POST', function ($objHaste) {
            return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
        }
        );

        $form->addFieldsFromDca(
            'tl_calendar_events_registration',
            function ($field, $config) {
                return isset($config['eval']['feEditable']);
            }
        );

        if ($event->er_register_multiple) {
            if ($attendeeFormConfig = $this->renderAddAttendeesFormConfig($event)) {
                $form->addFormField(
                    'add_attendees',
                    array(
                        'inputType' => 'add_attendee',
                        'label'     => $GLOBALS['TL_LANG']['fe_event_registration']['add_attendee_label'],
                        'eval'      => array(
                            'columnFields' => $attendeeFormConfig
                        )
                    )
                );
            }
        }

        $form->addSubmitFormField('register', $GLOBALS['TL_LANG']['fe_event_registration']['register']);
        $form->setNoValidate(true);
        $form->generateNoValidate();
        return $form;
    }

    protected function renderAddAttendeesFormConfig(\CalendarEventsModel $event)
    {
        \Controller::loadDataContainer('tl_calendar_events_registration');

        $allowedFieldConfig =
            deserialize($event->er_register_multiple_fields ?: $this->er_register_multiple_fields, true);

        if (!$allowedFieldConfig) {
            return null;
        }

        \Controller::loadLanguageFile('tl_calendar_events_registration');
        \Controller::loadDataContainer('tl_calendar_events_registration');

        $feEditableFields = array();
        foreach ($allowedFieldConfig as $field) {
            $fieldConfig = $GLOBALS['TL_DCA']['tl_calendar_events_registration']['fields'][$field];

            $feEditableFields[$field] = $fieldConfig;
        }

        return $feEditableFields;


    }

    protected function createRegistration($data, Form $form, \CalendarEventsModel $event)
    {
        $registration                      = new CalendarEventsRegistrationModel();
        $registration->pid                 = $event->id;
        $registration->tstamp              = time();
        $registration->registration_tstamp = time();
        $tokens                            = array();

        foreach ($form->getFormFields() as $field => $config) {
            if (!$config['feEditable']) {
                continue;
            }

            $registration->$field   = $data[$field];
            $attendeeTokens[$field] = $data[$field];
        }

        $registration->optout_hash = md5($registration->tstamp * rand(0, 999) * rand(43, 567));
        $registration->save();

        $notification = Notification::findByPk($event->er_notification ?: $this->er_notification);
        if (null !== $notification) {
            $tokens   = array();
            $eventRow = $event->row();

            StringUtil::flatten($attendeeTokens, 'attendee', $tokens);
            StringUtil::flatten($eventRow, 'event', $tokens);

            $tokens['optout'] = $this->generateOptSuccessoutUrl($event) . '?optOutHash=' . $registration->optout_hash;

            $notification->send($tokens);
        }

        if ($event->er_register_multiple && count($data['add_attendees']) > 0) {

            $submitted =
                array_filter($data['add_attendees'][0]);

            if ($submitted) {
                $this->createSubRegistration($data['add_attendees'], $event, $tokens);
            }
        }

        \Controller::redirect($this->generateSuccessUrl($event));
    }

    protected function createSubRegistration($data, \CalendarEventsModel $event, $mainAttendeeTokens)
    {
        foreach ($data as $attendee) {
            $tokens                            = $mainAttendeeTokens;
            $subAttendeeTokens                 = array();
            $registration                      = new CalendarEventsRegistrationModel();
            $registration->pid                 = $event->id;
            $registration->tstamp              = time();
            $registration->registration_tstamp = time();

            $attendeeFormConfig = $this->renderAddAttendeesFormConfig($event);
            foreach ($attendeeFormConfig as $field => $config) {
                if (!$attendee['attendee_' . $field]) {
                    continue;
                }
                $registration->$field      = $attendee['attendee_' . $field];
                $subAttendeeTokens[$field] = $attendee['attendee_' . $field];
            }

            $registration->optout_hash = md5($registration->tstamp * rand(0, 999) * rand(43, 567));
            $registration->save();

            $notification =
                Notification::findByPk($event->er_notification_attendee ?: $this->er_notification_attendee);
            if (null !== $notification) {

                StringUtil::flatten($subAttendeeTokens, 'sub_attendee', $tokens);

                $tokens['optout'] =
                    $this->generateOptSuccessoutUrl($event) . '?optOutHash=' . $registration->optout_hash;
                $notification->send($tokens);
            }
        }
    }

    protected function generateSuccessUrl(\CalendarEventsModel $event)
    {
        $page = $event->er_success ?: $this->er_success;

        if (!$page) {
            return null;
        }

        return $this->generateUrl($page);
    }

    protected function generateOptSuccessoutUrl(\CalendarEventsModel $event)
    {
        $page = $event->er_success_optout ?: $this->er_success_optout;

        if (!$page) {
            return null;
        }

        return $this->generateUrl($page);
    }

    protected function generateUrl($page)
    {
        $page = \PageModel::findWithDetails($page);

        if ($page === null) {
            return null;
        }

        return \Environment::get('url') . '/' . \Controller::generateFrontendUrl($page->row(), '', $page->language);
    }

    protected function getDatabase()
    {
        return $GLOBALS['container']['database.connection'];
    }
}