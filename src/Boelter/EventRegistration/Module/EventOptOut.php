<?php

namespace Boelter\EventRegistration\Module;

use Boelter\EventRegistration\Model\CalendarEventsRegistrationModel;
use Haste\Util\StringUtil;
use NotificationCenter\Model\Notification;

class EventOptOut extends \Module
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_event_optout';

    protected function compile()
    {
        \Controller::loadLanguageFile('fe_event_registration');
        $hash = \Input::get('optOutHash');

        if (!$hash) {
            $this->Template->errorMessage = $GLOBALS['TL_LANG']['fe_event_registration']['optout_error_hash'];
            return;
        }

        $registration = CalendarEventsRegistrationModel::findByHash($hash);

        if ($registration === null) {
            $this->Template->errorMessage = $GLOBALS['TL_LANG']['fe_event_registration']['optout_error'];
            return;
        }

        $event                          = \CalendarEventsModel::findByPk($registration->pid);
        $this->Template->successMessage =
            sprintf($GLOBALS['TL_LANG']['fe_event_registration']['optout_success'], $event->title);

        $notification =
            Notification::findByPk($this->er_optout_notification);
        if (null !== $notification) {
            $tokens = array();
            StringUtil::flatten($event->row(), 'event', $tokens);
            StringUtil::flatten($registration->row(), 'attendee', $tokens);
            
            $notification->send($tokens);
        }

        $registration->delete();
    }

    protected function getDatabase()
    {
        return $GLOBALS['container']['database.connection'];
    }
}