<?php

namespace Boelter\EventRegistration\Dca;

use Boelter\EventRegistration\Model\CalendarEventsRegistrationModel;
use Haste\IO\Mapper\DcaFieldMapper;
use Netzmacht\Contao\Toolkit\Dca\Callbacks;

class CalendarEventsRegistration extends Callbacks
{

    protected static $name = 'tl_calendar_events_registration';

    public function listEventRegistrations($row)
    {
        return $row['firstname'] . ' ' . $row['lastname']
               . ($row['company'] ? '<span style="color: #e3e3e3;"> [' . $row['company']
                                    . ']' : '');
    }

    public function generateRegistrationExport()
    {
        $registrations = CalendarEventsRegistrationModel::findByPid(
            \Input::get('id'),
            array(
                'order' => 'id ASC'
            )
        );

        if (!$registrations) {
            \Controller::redirect(ampersand(\Environment::get('request')));
        }

        $reader = new \Haste\IO\Reader\ModelCollectionReader($registrations);

        $mapper = new DcaFieldMapper('tl_calendar_events_registration');
        $mapper->setPreserveUnmapped(false);

        $mappedFields = $mapper->getMap();
        $headerFields = $this->renderHeaderFields($mappedFields);
        $reader->setHeaderFields($headerFields);

        $writer = new \Haste\IO\Writer\CsvFileWriter();
        $writer->enableHeaderFields();
        $writer->setRowCallback(
            function ($row) {
                return array(
                    $row[2],
                    $row[3],
                    $row[6],
                    $row[4],
                    $row[5],
                    \Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $row[1]),
                );
            }
        );

        $writer->setDelimiter(';');
        $writer->setMapper($mapper);
        $writer->writeFrom($reader);

        $objFile = new \File($writer->getFilename());
        $objFile->sendToBrowser();
    }

    protected function getDatabase()
    {
        return $GLOBALS['container']['database.connection'];
    }

    public function renderHeaderFields($mappedFields)
    {
        \Controller::loadLanguageFile('tl_calendar_events_registration');
        $headerFields = array();

        foreach ($mappedFields as $field => $sorting) {
            $headerFields[] = $GLOBALS['TL_LANG']['tl_calendar_events_registration'][$field][0];
        }

        return $headerFields;
    }

    public function modifyTableHeader($fields, \DataContainer $dataContainer)
    {
        \Controller::loadLanguageFile('tl_calendar_events_registration');

        $newFields            = array();
        $database             = $this->getDatabase();
        $currentRegistrations =
            $database->prepare("SELECT count(id) as registrations FROM tl_calendar_events_registration Where pid = ?")
                ->execute(
                    \Input::get('id')
                )->registrations;
        $maxRegistrations     =
            $database->prepare("SELECT er_max FROM tl_calendar_events Where id = ?")->limit(1)->execute(
                \Input::get('id')
            )->er_max;

        $newFields[$GLOBALS['TL_LANG']['tl_calendar_events_registration']['current_registrations'][0]]   = $currentRegistrations;
        $newFields[$GLOBALS['TL_LANG']['tl_calendar_events_registration']['available_registrations'][0]] =
            ($maxRegistrations - $currentRegistrations);

        return array_merge($fields, $newFields);
    }
}