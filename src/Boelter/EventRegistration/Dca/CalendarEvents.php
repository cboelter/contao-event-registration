<?php

namespace Boelter\EventRegistration\Dca;

use Netzmacht\Contao\Toolkit\Dca\Callbacks;

class CalendarEvents extends Callbacks
{

    protected static $name = 'tl_calendar_events';

    public function getNotifications()
    {
        $database     = $this->getDatabase();
        $options      = array();
        $notification =
            $database->prepare("SELECT n.id, n.title FROM tl_nc_notification AS n Where type = ?")->execute(
                'event_registration_main_attendee'
            );

        if ($notification->numRows == 0) {
            return $options;
        }

        while ($notification->next()) {
            $options[$notification->id] = $notification->title;
        }

        return $options;
    }

    public function getNotificationsAttendee()
    {
        $database     = $this->getDatabase();
        $options      = array();
        $notification =
            $database->prepare("SELECT n.id, n.title FROM tl_nc_notification AS n Where type = ?")->execute(
                'event_registration_attendee'
            );

        if ($notification->numRows == 0) {
            return $options;
        }

        while ($notification->next()) {
            $options[$notification->id] = $notification->title;
        }

        return $options;
    }

    public function generateEventRegistrationButton($row, $href, $label, $title, $icon, $attributes)
    {
        return $row['er_add_registration'] ? '<a href="' . \Controller::addToUrl($href . '&id=' . $row['id'])
                                             . '" title="' . specialchars($title)
                                             . '"'
                                             . $attributes . '>' . \Image::getHtml($icon, $label) . '</a> ' : '';
    }

    public function generateExportRegistrationButton($row, $href, $label, $title, $icon, $attributes)
    {
        return $row['er_add_registration'] ? '<a href="' . \Controller::addToUrl($href . '&id=' . $row['id'])
                                             . '" title="' . specialchars($title)
                                             . '"'
                                             . $attributes . '>' . \Image::getHtml($icon, $label) . '</a> ' : '';
    }

    public function getRegisterMultipleFields()
    {
        \Controller::loadLanguageFile('tl_calendar_events_registration');
        \Controller::loadDataContainer('tl_calendar_events_registration');

        $dataContainerFields = $GLOBALS['TL_DCA']['tl_calendar_events_registration']['fields'];
        $feEditableFields    = array();

        foreach ($dataContainerFields as $fieldName => $fieldConfig) {
            if (!$fieldConfig['eval']['feEditable']) {
                continue;
            }
            $feEditableFields[$fieldName] = $GLOBALS['TL_LANG']['tl_calendar_events_registration'][$fieldName][0];
        }

        return $feEditableFields;
    }

    protected function getDatabase()
    {
        return $GLOBALS['container']['database.connection'];
    }
}