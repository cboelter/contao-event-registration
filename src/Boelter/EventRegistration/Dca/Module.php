<?php

namespace Boelter\EventRegistration\Dca;

use Netzmacht\Contao\Toolkit\Dca\Callbacks;

class Module extends Callbacks
{
    protected static $name = 'tl_module';

    public function getOptOutNotifications()
    {
        $database     = $this->getDatabase();
        $options      = array();
        $notification =
            $database->prepare("SELECT n.id, n.title FROM tl_nc_notification AS n Where n.type = ?")->execute(
                'event_registration_optout'
            );

        if ($notification->numRows == 0) {
            return $options;
        }

        while ($notification->next()) {
            $options[$notification->id] = $notification->title;
        }

        return $options;
    }

    protected function getDatabase()
    {
        return $GLOBALS['container']['database.connection'];
    }
}